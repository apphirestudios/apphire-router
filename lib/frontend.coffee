EventEmitter2 = require('eventemitter2').EventEmitter2
queryStringParser = require('query-string')
chai = require 'chai'
expect = chai.expect #TODO switch to more lightweight assertion library
async = require 'co'

isPlainObject = require 'is-plain-object'
isFunction = (obj) ->
  return not not (obj and obj.constructor and obj.call and obj.apply)

class DefinitionError extends Error
  constructor: (@message)-> super()

class RuntimeError extends Error
  constructor: (@message)-> super()

checkRoutesDescription = (routeDescription, currentHash)->
  throwError = (msg)->
    throw new DefinitionError 'Error defining route "' + (currentHash or '/') + '": ' + msg
  itIsLastRoute = true #This will be set to false if we'll get any child route definitions during parsing
  try
    expect(routeDescription).to.be.an 'object'
  catch e
    throwError 'Route definition should be an object, instead got ' + routeDescription
  if not routeDescription.module? and not routeDescription.redirect?
    throwError 'Description of route must have either "module" or "redirect" attribute'

  for k, v of routeDescription
    switch k
      when 'redirect'
        try
          expect(v).to.be.a 'string'
        catch e
          throwError '"Redirect" attribute should be a string, instead got:  "' + v + '"'
        try
          expect(routeDescription[v]).to.be.a 'object'
        catch e
          throwError 'You are trying to redirect to route ' + v + ' which does not exist'
        try
          expect(routeDescription.module).to.not.exist
        catch e
          throwError 'You cannot have both "redirect" and "module" defined in same route'
      when 'module'
        if isPlainObject(v) or isFunction(v)
          if isPlainObject(v)
            if not v.view?
              throwError 'Module definition should have "view" attribute'
            else if not isFunction(v.view)
              throwError 'Module\'s "view" attribute should be a function'
            if v.controller? and not  isFunction(v.controller)
              throwError 'Module\'s "controller" attribute should be a function'
        else
          throwError 'Module definition should be either object or function'

        try
          expect(routeDescription.redirect).to.not.exist
        catch e
          throwError 'You cannot have both "redirect" and "module" defined in same route'
      when 'before'
        try
          expect(v).to.be.an 'array'
        catch e
          throwError '"Before" attribute should be an array, instead got:  "' + v + '"'


      else
        if k[0] is ':'
          for k1, v1 of routeDescription when k isnt k1 and k1[0] is ':'
            throwError 'You are trying to define two parameterized routes: ' + k1 + ' and ' + k
        checkRoutesDescription(v, (currentHash or '') + '/' + k)


getParameterizedRouteName = (desc)->
  for k, v of desc
    if k[0] is ':' then return k
  return undefined

class Router extends EventEmitter2
  DefinitionError: DefinitionError
  RuntimeError: RuntimeError
  autostart: true
  navigateCallIndex: 0
  #logic follows - call to navigate method could
  #happen inside beforeFunction of CURRENT navigate method while we resolve route
  #we need to immediately stop executing of previous navigate method
  constructor: (@routes)->
    checkRoutesDescription(@routes)
    if Router.autostart?
      @autostart = Router.autostart
      @start()
    super
      wildcard: true
      delimiter: ':'


  _startHashChangeListener: =>
    window.onhashchange = =>
      @navigate window.location.hash
  start: ->
    @_startHashChangeListener()
    @on 'change', (route)->
      window.onhashchange =null
      window.location.hash = route.hash
      setTimeout @_startHashChangeListener, 10

  navigate: (hash) -> currentIndex = ++@navigateCallIndex; async =>

    hash = hash.replace(/(\/*)$/, "") #stripping slashes
    @params = {}
    @query = {}
    #we get hash like #/first/second so we need to strip hashbang and first slash
    #fragments = hash.substring(1).split('/')
    if hash[0] isnt '/' then hash = '/' + hash

    fragments = hash.split('/')
    current = @routes
    beforeFunctions = []
    for fragment, index in fragments
      if current.before?
        for fn in current.before
          beforeFunctions.push fn
      if index isnt fragments.length - 1
        nextFragment = (fragments[index+1]).split('?')[0] #Stripping query string
        if current[nextFragment]?
          current = current[nextFragment]
        else if (parameterizedRouteName = getParameterizedRouteName(current))?
          current = current[parameterizedRouteName]
          @params[parameterizedRouteName.substring(1)] = nextFragment
        else
          @emit 'error', new RuntimeError 'Route name ' + hash + ' not found'
          throw new RuntimeError 'Route name ' + hash + ' not found'
      else
        if current.redirect?
          #currentHash = '#' + (fragment for fragment, i in fragments when i <= index).join('/')
          #return @navigate(currentHash + '/' + current.redirect)
          hash = hash + '/' + current.redirect
          current = current[current.redirect]
        queryString = fragment.split('?')[1]
        if queryString? then @query = queryStringParser.parse(queryString)
        @route = hash
        for fn in beforeFunctions
          yield async fn
          if currentIndex isnt @navigateCallIndex then return
        result =
          module: current.module
          hash: hash
        @emit 'change', result
        return result
module.exports = Router