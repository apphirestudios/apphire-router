describe 'Apphire-Router', ->
  describe 'Requiring...', ->
    it 'throws wnen trying to require router from server', ->
      (-> require('../index')).should.Throw(/This module is clientside only/)

    it 'requires router from client correctly', ->
      global.window = {}
      require '../index'
      global.window = undefined

  describe 'Instantiating...', ->
    Router = undefined

    it 'BEFORE: instantiates router class', ->
      global.window = {}
      Router = require '../index'
      Router.autostart = false #This prevents router from listening window.hash changes

    it 'throws wnen routes are empty', ->
      (-> new Router()).should.Throw(Router.DefinitionError, /object/)

    it 'throws wnen no module or redirect in some of the routes', ->
      (-> new Router({})).should.Throw(Router.DefinitionError, /redirect/)

    it 'throws wnen both module and redirect has been defined', ->
      (-> new Router({module: (->), redirect: 'some'})).should.Throw(Router.DefinitionError, /You cannot have both "redirect" and "module"/)
      (-> new Router(redirect: 'some', some: {module: (->), redirect: 'some1'})).should.Throw(Router.DefinitionError, /You cannot have both "redirect" and "module"/)

    it 'throws wnen nowhere to redirect', ->
      (-> new Router({redirect: 'some'})).should.Throw(Router.DefinitionError, /You are trying to redirect to route/)
      (-> new Router({redirect: 'some', some: redirect: 'some2'})).should.Throw(Router.DefinitionError, /You are trying to redirect to route/)

    it 'throws wnen module definition is wrong', ->
      (-> new Router({module: 'some'})).should.Throw(Router.DefinitionError, /should be either object or function/)
      (-> new Router({module: controller: {}})).should.Throw(Router.DefinitionError, /should have "view" attribute/)
      (-> new Router({module: controller: {}, view: {}})).should.Throw(Router.DefinitionError, /should be a function/)
      (-> new Router({module: controller: {}, view: {}})).should.Throw(Router.DefinitionError, /should be a function/)
      (-> new Router({redirect: 'some', 'some': module: new Date()})).should.Throw(Router.DefinitionError, /should be either object or function/)

    it 'throws wnen redirect definition is wrong', ->
      (-> new Router({redirect: {}})).should.Throw(Router.DefinitionError, /"Redirect"/)
      (-> new Router({redirect: 'some', 'some': {redirect: new Date()}})).should.Throw(Router.DefinitionError, /"Redirect"/)

    it 'throws wnen two parameterized routes in set', ->
      (-> new Router({module: (->), ':one': {module: (->)}, ':two': {module: (->)} })).should.Throw(Router.DefinitionError, /two parameterized routes/)


  describe 'Routing...', ->
    Router = undefined

    it 'BEFORE: instantiates router class', ->
      global.window = {}
      Router = require '../index'
      Router.autostart = false #This prevents router from listening window.hash changes
    router = firstModule1 = firstModule2 = secondModule1 = secondModule2 = articleModule = pageModule = undefined

    it 'BEFORE: instantiates router instance', ->
      firstModule1 = ->
      firstModule2 = ->
      secondModule1 = ->
      secondModule2 = ->
      articleModule = ->
      pageModule = ->

      router = new Router
        redirect: 'first1'
        first1:
          module: firstModule1
        first2:
          redirect: 'second2'
          second1:
            module: secondModule1
          second2:
            module: secondModule2
        articles:
          redirect: ':articleId'
          ':articleId':
            redirect: 'edit'
            edit:
              module: articleModule
              pages:
                redirect: ':pageId'
                ':pageId':
                    module: pageModule

    it 'routes to', ->
      (yield router.navigate('#/first1')).module.should.equal firstModule1
      (yield router.navigate('#/first1')).hash.should.equal '#/first1'

    it 'strips slashes', ->
      (yield router.navigate('#/first1///')).module.should.equal firstModule1
      (yield router.navigate('#/first1///')).hash.should.equal '#/first1'
      (yield router.navigate('#////')).hash.should.equal '#/first1'

    it 'throws when route not found', ->
      yield throws router.navigate('#/firstERRRRR'), Router.RuntimeError, /not found/

    it 'routes to with redirection', ->
      (yield router.navigate('#')).module.should.equal firstModule1
      (yield router.navigate('#')).hash.should.equal '#/first1'
      (yield router.navigate('#/first2')).module.should.equal secondModule2
      (yield router.navigate('#/first2')).hash.should.equal '#/first2/second2'

    it 'routes to parameterized', ->
      (yield router.navigate('#/articles/id123/edit')).module.should.equal articleModule
      (yield router.navigate('#/articles/id123/edit')).hash.should.equal '#/articles/id123/edit'
      (yield router.navigate('#/articles/id123')).module.should.equal articleModule
      (yield router.navigate('#/articles/id123')).hash.should.equal '#/articles/id123/edit'

    it 'parses params', ->
      yield router.navigate('#/articles/id1/edit')
      router.params.articleId.should.equal 'id1'
      yield router.navigate('#/articles/id2/edit/pages/id3')
      router.params.articleId.should.equal 'id2'
      router.params.pageId.should.equal 'id3'
      yield router.navigate('#/first1')
      router.params.should.be.empty

    it 'parses query string', ->
      yield router.navigate('#/first1?one=1&two=two')
      router.query.one.should.equal '1'
      router.query.two.should.equal 'two'

